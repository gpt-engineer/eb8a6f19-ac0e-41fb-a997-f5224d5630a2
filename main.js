// Task list array to store tasks
let tasks = [];

// Function to render the task list
function renderTasks() {
  const taskList = document.getElementById("taskList");
  taskList.innerHTML = "";

  tasks.forEach((task, index) => {
    const taskItem = document.createElement("div");
    taskItem.classList.add(
      "flex",
      "items-center",
      "justify-between",
      "py-2",
      "border-b",
      "border-gray-300"
    );

    const taskText = document.createElement("span");
    taskText.textContent = task;
    taskText.classList.add("text-lg");

    const taskActions = document.createElement("div");
    taskActions.classList.add("flex", "items-center");

    const completeButton = document.createElement("button");
    completeButton.innerHTML = '<i class="fas fa-check"></i>';
    completeButton.classList.add(
      "mr-2",
      "px-2",
      "py-1",
      "bg-green-500",
      "text-white",
      "rounded-md"
    );
    completeButton.addEventListener("click", () => completeTask(index));

    const deleteButton = document.createElement("button");
    deleteButton.innerHTML = '<i class="fas fa-trash"></i>';
    deleteButton.classList.add(
      "px-2",
      "py-1",
      "bg-red-500",
      "text-white",
      "rounded-md"
    );
    deleteButton.addEventListener("click", () => deleteTask(index));

    taskActions.appendChild(completeButton);
    taskActions.appendChild(deleteButton);

    taskItem.appendChild(taskText);
    taskItem.appendChild(taskActions);

    taskList.appendChild(taskItem);
  });
}

// Function to add a new task
function addTask(event) {
  event.preventDefault();
  const taskInput = document.getElementById("taskInput");
  const task = taskInput.value.trim();

  if (task !== "") {
    tasks.push(task);
    taskInput.value = "";
    renderTasks();
  }
}

// Function to mark a task as completed
function completeTask(index) {
  tasks.splice(index, 1);
  renderTasks();
}

// Function to delete a task
function deleteTask(index) {
  tasks.splice(index, 1);
  renderTasks();
}

// Event listener for form submission
const addTaskForm = document.getElementById("addTaskForm");
addTaskForm.addEventListener("submit", addTask);

// Initial rendering of tasks
renderTasks();
